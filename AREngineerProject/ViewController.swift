//
//  ViewController.swift
//  AREngineerProject
//
//  Created by Роман Хоменко on 27.01.2022.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var sizeModelSlider: UISlider! {
        didSet {
            sizeModelSlider.maximumValue = 0.2
            sizeModelSlider.minimumValue = 0
            sizeModelSlider.value = 0.05
        }
    }
    
    var objectArray = [SCNNode]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        
        // Set the view's delegate
        sceneView.delegate = self
        
        
        sceneView.autoenablesDefaultLighting = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Surface detection
        configuration.planeDetection = .horizontal
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    // MARK: - Object Rendering Methods
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let touchLocatin = touch.location(in: sceneView)
            
            let results = sceneView.hitTest(touchLocatin, types: .existingPlaneUsingExtent)
            
            if let hitResult = results.first {
                if objectArray.count >= 1 {
                    objectArray.last!.removeFromParentNode()
                    objectArray.removeLast()
                    addObject(atLocation: hitResult)
                } else {
                    addObject(atLocation: hitResult)
                }
            }
        }
    }
    

    
    func addObject(atLocation location: ARHitTestResult) {
    // Create a new scene with touch
        let objectScene = SCNScene(named: "art.scnassets/diesel_Generator.scn")!
        
        if let objectNode = objectScene.rootNode.childNode(withName: "DieselGenerator", recursively: true) {
            objectNode.position = SCNVector3(location.worldTransform.columns.3.x,
                                           location.worldTransform.columns.3.y,
                                           location.worldTransform.columns.3.z
                                            )
            objectArray.append(objectNode)
            objectArray[0].scale.x = Float(sizeModelSlider.value)
            objectArray[0].scale.y = Float(sizeModelSlider.value)
            objectArray[0].scale.z = Float(sizeModelSlider.value)
            
            sceneView.scene.rootNode.addChildNode(objectNode)
        }
    }
    
    @IBAction func changeObjectScale(_ sender: UISlider) {
        guard !objectArray.isEmpty else { return }
        
        objectArray[0].scale.x = Float(sizeModelSlider.value)
        objectArray[0].scale.y = Float(sizeModelSlider.value)
        objectArray[0].scale.z = Float(sizeModelSlider.value)
    }

        
    @IBAction func removeLastObjectButton(_ sender: UIBarButtonItem) {
        if !objectArray.isEmpty {
            objectArray.last!.removeFromParentNode()
            objectArray.removeLast()
        }
    }
    
    
    @IBAction func segueInfoButton(_ sender: UIBarButtonItem) {
        if let url = URL(string: "https://holod-magazin.ru/compressors/kompressor-bitzer-ecoline-8ge-50y-40p.html") {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
    }
    
    // MARK: - Move model buttons
    
//    @IBAction func moveLeftButton(_ sender: UIButton) {
//        guard !objectArray.isEmpty else { return }
//        // need to rotate model
//
//        objectArray[0].rotation.w = 1
//        objectArray[0].rotation.y -= 0.01
//    }
//
//    @IBAction func moveRightButton(_ sender: UIButton) {
//        guard !objectArray.isEmpty else { return }
//        // need to rotate model
//
//        objectArray[0].rotation.w = 1
//        objectArray[0].rotation.y += 0.01
////        = SCNVector4(0, 0.5, 0, 1)
//    }
//
//    @IBAction func moveForwardButton(_ sender: UIButton) {
//        guard !objectArray.isEmpty else { return }
//
//        objectArray[0].position.z += 0.01
//
//    }
//
//    @IBAction func moveBackButton(_ sender: UIButton) {        guard !objectArray.isEmpty else { return }
//
//        objectArray[0].position.z -= 0.01
//
//    }
    
    // MARK: - ARSceneViewdelegateMethods
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        // Chek existence of surface detection method
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        let planeNode = createPlane(withPlaneAnchor: planeAnchor)
        
        node.addChildNode(planeNode)
    }
    
    // MARK: - Plane Rendering Methods
    func createPlane(withPlaneAnchor planeAnchor: ARPlaneAnchor) -> SCNNode {
        let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
        let planeNode = SCNNode()
        planeNode.position = SCNVector3(planeAnchor.center.x, 0, planeAnchor.center.z)
        planeNode.transform = SCNMatrix4MakeRotation(-Float.pi/2, 1, 0, 0)
        let gridMaterial = SCNMaterial()
        gridMaterial.diffuse.contents = UIImage(named: "art.scnassets/grid.png")
        plane.materials = [gridMaterial]
        planeNode.geometry = plane
        
        return planeNode
    }
}
